#! /usr/bin/python3

# required to get the new IP
import requests
import json

# Please update this parameter if you intend to use a different file
param_file = "extipcheckv2.cfg"
# Please edit the above file and adopt it to your needs. Comments in the file will help.

# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText

# Imports for sending mail
import sys
import os
import re

from smtplib import SMTP_SSL as SMTP  # this invokes the secure SMTP protocol (port 465, uses SSL)

from email.mime.text import MIMEText

import datetime

# Get subdomain data from dynu.com
def get_subdomain_data(subdomain, api_key):
    endpoint = f"https://api.dynu.com/v2/dns/record/{subdomain}?recordType=A"
    headers = {"API-Key": api_key}
    get_response = requests.get(endpoint, headers=headers)
    if get_response.status_code == 200:
        data = {
                "nodeName": get_response.json()['dnsRecords'][0]['nodeName'],
                "recordType": get_response.json()['dnsRecords'][0]['recordType'],
                "ttl": get_response.json()['dnsRecords'][0]['ttl'],
                "state": get_response.json()['dnsRecords'][0]['state'],
                "ipv4Address": get_response.json()['dnsRecords'][0]['ipv4Address'],
                "domainId": get_response.json()['dnsRecords'][0]['domainId'],
                "id": get_response.json()['dnsRecords'][0]['id']
            }
        return(data)
    else:
        return(False)

# Get subdomain data from dynu.com
def update_subdomain_ip(subdomain_data, api_key, new_ip):

    domain_id = subdomain_data["domainId"]
    dns_record_id = subdomain_data["id"]
    
    endpoint = f"https://api.dynu.com/v2/dns/{domain_id}/record/{dns_record_id}"
    
    headers = {
        "accept": "application/json",
        "Content-Type": "application/json",
        "API-Key": f"{api_key}"
        }

    data = {
            "nodeName": subdomain_data["nodeName"],
            "recordType": subdomain_data["recordType"],
            "ttl": subdomain_data["ttl"],
            "state": subdomain_data["state"],
            "ipv4Address": new_ip
        }

    update_response = requests.post(endpoint, headers=headers, data=json.dumps(data))
    
    return(update_response)

# Saving the IP. Time stamp used only for logging
def save_ip(ip, ct, ip_file):
    print("{} >> Saving new IP".format(ct))
    f = open(os.path.join(os.path.dirname(__file__), ip_file), "w")
    f.write(format(ip))
    f.close()
    print("{} >> New IP saved!".format(ct))

def is_valid_ipv4_address(address):
    octets = address.split('.')
    if len(octets) != 4:
        return False
    for octet in octets:
        if not octet.isdigit():
            return False
        if int(octet) < 0 or int(octet) > 255:
            return False
    return True

def get_old_ip(file):
    try:
        f = open(os.path.join(os.path.dirname(__file__), file), "r")
        ip = f.read()
        f.close()
    except IOError:
        ip = ""
    return ip

def build_content(ct, old_ip, ip):
    content = "{} >> IP update triggered! \n Old IP: {} \n New IP: {}\n".format(ct, old_ip, ip)
    content_html = "{} <br> IP update triggered! <br> Old IP: {} <br> New IP: {} <br>".format(ct, old_ip, ip)
    return (content, content_html)

def send_email(subject, content, smtp_send_mail, smtp_server, smtp_sender, smtp_password, smtp_destination, smtp_sender_user):

    # typical values for text_subtype are plain, html, xml
    text_subtype = 'html'

    # send email only when smtp_send_mail is set to true in the parameters file
    if smtp_send_mail:            
        msg = MIMEText(content, text_subtype)
        msg['Subject'] = subject
        msg['From'] = smtp_sender  # some SMTP servers will do this automatically, not all
        try:
            conn = SMTP(smtp_server)
            conn.set_debuglevel(False)
            conn.login(smtp_sender_user, smtp_password)
            try:
                conn.sendmail(smtp_sender, smtp_destination, msg.as_string())
                # print("{} >> Email sent!".format(cp))
            finally:
                conn.quit()

        except:
            sys.exit("mail failed; %s" % "CUSTOM_ERROR")  # give an error message  

def get_external_ip(ip_source_site):
    ip = requests.get(ip_source_site).content.decode('utf8')
    if is_valid_ipv4_address(ip):
        return(ip)
    else:
        return(False)

def get_script_params(filename):
    params = {}
    params["subdomains"] = []

    with open(os.path.join(os.path.dirname(__file__), filename)) as file:
        while line := file.readline():
            l = line.rstrip()
            if len(l) > 0 and l[0] == "#":
                continue
            elif len(l) > 0:
                p = l.split("#",1)[0]
                if "=" in p:
                    p_name, p_val = p.split("=",1)
                    params[p_name] = p_val
                else:
                    params["subdomains"].append(p)

    return params

def update_subdomains(subdomains, api_key, new_ip):

    content = "This is the result of the subdomains update attempt:"

    for subdomain in subdomains:
        subdomain_data = get_subdomain_data(subdomain, api_key)
        if subdomain_data["ipv4Address"] != new_ip:
            update_response = update_subdomain_ip(subdomain_data, api_key, new_ip)
            if update_response.status_code == 200:
                content += "<br>Subdomain {} was updated to the new IP Address {}".format(subdomain, new_ip)
            else:
                content += "<br>Subdomain {} was updated to the new IP Address {}. The error code is: {}".format(subdomain, 
                        new_ip, update_response.status_code)
        else:
            content += "<br>Subdomain {} has already the new IP Address {}. Was it manually updated?".format(subdomain, new_ip)

    return(content)

############### Main program
# get time stamp
ct = datetime.datetime.now()
#print(ct)
# get parameters from parameters file
params = get_script_params(param_file)

# comment if necessary
# print(params)

if params["smtp_send_mail"]:
    smtp_send_mail = params["smtp_send_mail"]
    smtp_server = params["smtp_server"]
    smtp_sender = params["smtp_sender"]
    if params["smtp_sender_user"]:
        smtp_sender_user = params["smtp_sender_user"]
    else:
        smtp_sender_user = smtp_sender
    smtp_password = params["smtp_password"]
    smtp_destination = params["smtp_destination"]

# check the external ip
new_ip = get_external_ip(params["ip_source_site"])
# comment if necessary
#print("This is the new ip: {}".format(new_ip))

if is_valid_ipv4_address(new_ip):

    old_ip = get_old_ip(params["ip_file"])
    # comment if necessary
    #print("This is the old ip: {}".format(old_ip))

    if not is_valid_ipv4_address(old_ip):
        print("Old IP is not valid")
        if is_valid_ipv4_address(new_ip):
            print("{} >> This seems to be the first check! IP {} will be saved and script will try to update the subdomains".format(ct, new_ip))
            save_ip(new_ip, ct, params["ip_file"])
            ## !!! here we need to update the IPs on dynu in case they do not match
            content, content_html = build_content(ct, "First run", new_ip)
            update_content = update_subdomains(params["subdomains"], params["api_key"], new_ip)

            content += update_content.replace("<br>", "\n")
            content_html += update_content

            # printing the result (can be logged)
            print(content)

            subject = "New external IP detected {}".format(new_ip)

            print("{} >> Sending mail.".format(ct))
            send_email(subject, content_html, smtp_send_mail, smtp_server, smtp_sender, 
                smtp_password, smtp_destination, smtp_sender_user)
            #send_email(subject, content_html)

    elif not is_valid_ipv4_address(new_ip):
            ## Sometimes the external IP is not correctly read. In case this happens too often, something else is broken
            print("{} >> External IP not valid. Skipping! A new try will be done at next iteration".format(ct))
    elif old_ip == new_ip:
            print("{} >> Same old IP: {}".format(ct, new_ip))
    else:
            print("{} >> New IP detected: {}. Script will try to update the subdomains".format(ct, new_ip))
            
            ## !!! here we need to update the IPs on dynu in case they do not match
            content, content_html = build_content(ct, old_ip, new_ip)
            update_content = update_subdomains(params["subdomains"], params["api_key"], new_ip)


            content += update_content.replace("<br>", "\n")
            content_html += update_content

            # printing the result (can be logged)
            print(content)

            subject = "New external IP detected {}".format(new_ip)

            print("{} >> Sending mail.".format(ct))

            send_email(subject, content_html, smtp_send_mail, smtp_server, smtp_sender, 
                smtp_password, smtp_destination, smtp_sender_user)
            #send_email(subject, content_html)

            save_ip(new_ip, ct, params["ip_file"])




