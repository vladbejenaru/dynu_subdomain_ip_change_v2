# Scope
This script updates the IP for the DDNS subdomains (aliases) managed by dynu.com using the V2 of the Dynu API.
This is useful for when one hosts different subdomains in different locations.

In my particular case I have services that are hosted in the cloud, where the IP does not change, but I host also couple of them on my home server. And on the home internet connection I do not have a fixed IP Address. Therfore I need to update the IP only for the services hosted at home.

Unfortunatelly ddclient does not manage to update more than one subdomain. After reporting this to dynu and not receiving any other solution I have wroten the actual script. Now I am making it public in the hope that it will help also someone else.

# Usage

1. Clone this repository
2. Adapt the `dynu_subdomain_ip_change_v2.cfg` file to match your requirements. The file has instructions in the commentary.
3. Make the .py file executable: `chmod +x dynu_subdomain_ip_change_v2.py`
4. Setup a cronjob to run the script on your desired frequency. Here is an example where the script is run every hour:
`0 * * * * /path/to/script/extipcheckv2.py >> /path/to/log/extipcheckv2.log 2>&1`
In this case the output of the script is also sent to a log file. Please note that there is no log rotate function in place `:-)`

# Conclusion
I hope it helps anybody on one side, I hope I will receive feedback and suggestions on the other side. This is also an exercise for me to get a bit back into scripting and using git, so please be gentle.

Happy selfhosting!
